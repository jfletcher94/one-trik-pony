﻿using System;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour {

    [SerializeField] private float buffer = 1f;
    [SerializeField] private PlayerMovement playerMovement;

    private Camera _camera;

    public float GetBuffer() {
        return buffer;
    }

    private void Awake() {
        _camera = GetComponent<Camera>();
    }

    private void Update() {
        float left = _camera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + GetBuffer();
        float right = _camera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - GetBuffer();
        float x = playerMovement.DistanceTraveled();
        
        if (x < left) {
            var cameraTransform = _camera.transform;
            var position = cameraTransform.position;
            cameraTransform.position = new Vector3(position.x + x -left, position.y, position.z);
        } else if (x > right) {
            var cameraTransform = _camera.transform;
            var position = cameraTransform.position;
            cameraTransform.position = new Vector3(position.x + x - right, position.y, position.z);
        }
    }

}
