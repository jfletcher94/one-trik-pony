﻿using TMPro;
using UnityEngine;

public class DifficultyDisplay : MonoBehaviour {

    [SerializeField] private float scoreMultiplier = 10f;
    [SerializeField] private PlayerMovement playerMovement;

    private TMP_Text _text;

    private void Awake() {
        _text = GetComponent<TMP_Text>();
    }

    private void Update() {
        _text.SetText($"{-(int) (scoreMultiplier * playerMovement.Difficulty())}");
    }

}
