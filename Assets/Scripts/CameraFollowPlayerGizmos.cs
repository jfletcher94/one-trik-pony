﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraFollowPlayerGizmos : MonoBehaviour {

    private Camera _camera;
    private CameraFollowPlayer _cameraFollowPlayer;

    private void Awake() {
        _camera = GetComponent<Camera>();
        _cameraFollowPlayer = GetComponent<CameraFollowPlayer>();
    }

    private void OnDrawGizmosSelected() {
        float left = _camera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + _cameraFollowPlayer.GetBuffer();
        float right = _camera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - _cameraFollowPlayer.GetBuffer();
        Gizmos.DrawLine(new Vector3(left, -10, 0), new Vector3(left, 10, 0));
        Gizmos.DrawLine(new Vector3(right, -10, 0), new Vector3(right, 10, 0));
    }

}
