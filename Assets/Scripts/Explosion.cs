﻿using UnityEngine;

public class Explosion : MonoBehaviour {

    [SerializeField] private float lifetime = 1f;
    [SerializeField] private float minScale = 0.25f;
    [SerializeField] private float maxScale = 3f;
    [SerializeField] private SpriteRenderer spriteRenderer;

    private SpriteRenderer _spriteRenderer;
    private float _age = 0f;

    private void Awake() {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update() {
        _age += Time.deltaTime;
        if (_age > lifetime) {
            Destroy(gameObject);
            return;
        }

        float normalizedAge = _age / lifetime;
        transform.localScale = new Vector3(normalizedAge * maxScale + minScale, normalizedAge * maxScale + minScale, 1);
        var color = _spriteRenderer.color;
        _spriteRenderer.color = new Color(color.r, color.g, color.b, 1 - normalizedAge);
    }

    public void SetColor(Color color) {
        spriteRenderer.color = color;
    }

}
