﻿using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] private GameObject explosionPrefab;
    [SerializeField] private SpriteRenderer spriteRenderer;

    private void Update() {
        if (transform.position.y < -5f) {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (!other.enabled) {
            return;
        }

        var myTransform = transform;
        var explosion = Instantiate(explosionPrefab, myTransform.position, myTransform.rotation, myTransform.parent);
        explosion.GetComponent<Explosion>().SetColor(spriteRenderer.color);
        Destroy(gameObject);
    }

    public void SetColor(Color color) {
        spriteRenderer.color = color;
    }

    public Color GetColor() {
        return spriteRenderer.color;
    }

}
