﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour {

    [SerializeField] private PlayerMovement playerMovement;

    private void Update() {
        var position = transform.position;
        transform.position = new Vector3(playerMovement.DistanceTraveled(), position.y, position.z);
    }

}
