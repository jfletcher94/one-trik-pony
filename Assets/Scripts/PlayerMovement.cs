﻿using TMPro;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private TMP_Text bestText;
    [SerializeField] private float acceleration = 1f;
    [SerializeField] private float jumpForce = 1f;
    [SerializeField] private float invincibilitySeconds = 1.5f;
    [SerializeField] private float hitPenalty = 16f;

    private Rigidbody2D _rigidbody;
    private float _difficulty = 0f;
    private float _maxDistanceTraveled = 0f;
    private bool _grounded = true;
    private bool _invincible = false;
    private float _invincibilityRemaining = 0f;
    private float _totalPenalty = 0f;
    private bool _hasMoved = false;

    public float DistanceTraveled() {
        return transform.position.x;
    }

    public float Difficulty() {
        return _difficulty + _maxDistanceTraveled;
    }

    private void Awake() {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate() {
        HandleJump();
        HandleMove();
    }

    private void Update() {
        if (Input.GetKey(KeyCode.Escape)) {
            Application.Quit();
        }
        _maxDistanceTraveled = Mathf.Max(_maxDistanceTraveled, DistanceTraveled());
        if (!_invincible) {
            return;
        }

        _invincibilityRemaining -= Time.deltaTime;
        if (_invincibilityRemaining <= 0f) {
            _invincible = false;
            spriteRenderer.enabled = true;
            scoreText.outlineWidth = 0;
            bestText.outlineWidth = 0;
        } else {
            spriteRenderer.enabled = !spriteRenderer.enabled;
            scoreText.outlineWidth = 1 - scoreText.outlineWidth;
            bestText.outlineWidth = scoreText.outlineWidth;
        }
    }

    private void HandleJump() {
        if (!_grounded) {
            return;
        }

        if (!Input.GetKey(KeyCode.Space)) {
            return;
        }

        _rigidbody.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
        _grounded = false;
        HandleFirstMove();
    }

    private void HandleMove() {
        float adjustedAcceleration = _grounded ? acceleration : acceleration * 0.75f;
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
            _rigidbody.AddForce(new Vector2(-adjustedAcceleration, 0f));
            HandleFirstMove();
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
            _rigidbody.AddForce(new Vector2(adjustedAcceleration, 0f));
            HandleFirstMove();
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (!other.enabled) {
            return;
        }

        Standable standable = other.collider.GetComponent<Standable>();
        if (standable != null) {
            _grounded = true;
            return;
        }

        if (_invincible) {
            return;
        }

        Enemy enemy = other.collider.GetComponent<Enemy>();
        if (enemy != null) {
            spriteRenderer.color = enemy.GetColor();
            scoreText.color = enemy.GetColor();
            bestText.color = enemy.GetColor();
            TeleportBack();
        }
    }

    private void TeleportBack() {
        var myTransform = transform;
        var position = myTransform.position;
        myTransform.position = new Vector3(position.x - (_totalPenalty += hitPenalty), position.y + 0.01f, position.z);
        _invincible = true;
        _invincibilityRemaining = invincibilitySeconds * (_totalPenalty / hitPenalty);
        _difficulty += _totalPenalty;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (!other.enabled || !other.CompareTag("InsidePlatform")) {
            return;
        }

        var myTransform = transform;
        var position = myTransform.position;
        myTransform.position = new Vector3(position.x, position.y + 2f, position.z);
    }

    private void HandleFirstMove() {
        if (!_hasMoved) {
            _hasMoved = true;
            scoreText.GetComponent<ScoreDisplay>().enabled = true;
            bestText.GetComponent<DifficultyDisplay>().enabled = true;
            GetComponent<EnemyGenerator>().enabled = true;
        }
    }

}
