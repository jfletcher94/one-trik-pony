﻿using UnityEngine;
using Random = System.Random;

public class EnemyGenerator : MonoBehaviour {

    [SerializeField] private float spawnRange = 24f;
    [SerializeField] private float spawnHeight = 5.2f;
    [SerializeField] private float spawnRateBase = 1f;
    [SerializeField] private float spawnRateScale = 0.1f;
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private GameObject enemyParent;

    private readonly Random _random = new Random();
    private PlayerMovement _playerMovement;
    private float _nextSpawn;

    private void Awake() {
        _playerMovement = GetComponent<PlayerMovement>();
        _nextSpawn = spawnRateBase;
    }

    private void Update() {
        _nextSpawn -= Time.deltaTime;
        if (_nextSpawn <= 0f) {
            Spawn();
            _nextSpawn = 1f / (spawnRateBase + spawnRateScale * _playerMovement.Difficulty());
        }
    }

    private void Spawn() {
        var color = new Color(RandomColorComponent(), RandomColorComponent(), RandomColorComponent(), 1f);
        float x = transform.position.x + (float) _random.NextDouble() * 2 * spawnRange - spawnRange;
        var enemy = Instantiate(
            enemyPrefab,
            new Vector3(x, spawnHeight, 0f),
            Quaternion.identity,
            enemyParent.transform);
        enemy.GetComponent<Enemy>().SetColor(color);
    }

    private float RandomColorComponent() {
        return _random.Next(128, 256) / 256f;
    }

}
