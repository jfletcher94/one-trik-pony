﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorldGenerator : MonoBehaviour {

    [SerializeField] private float chunkRadius = 8f; // total chunk size = 2 * chunkRadius
    [SerializeField] private int chunkKeepRadius = 3; // total chunks kept = 2 * chunkKeepRadius + 1
    [SerializeField] private GameObject platformPrefab;
    [SerializeField] private GameObject platformParent;

    private int _seed;
    private int _currentChunk = 0;
    private float _minChunkWall;
    private float _maxChunkWall;
    private readonly Dictionary<int, GameObject[]> _chunkMap = new Dictionary<int, GameObject[]>();

    private void Awake() {
        _seed = Environment.TickCount % 2003 + 997;
        _minChunkWall = -chunkRadius;
        _maxChunkWall = chunkRadius;
        UpdateCurrentChunk();
    }

    private void Update() {
        float x = transform.position.x;
        if (!(x < _minChunkWall) && !(x > _maxChunkWall)) {
            return;
        }

        _currentChunk = (int) ((x + chunkRadius) / (2f * chunkRadius));
        UpdateCurrentChunk();
    }

    private void UpdateCurrentChunk() {
        foreach (var chunk in _chunkMap.Keys.ToList()
            .Where(chunk => chunk < _currentChunk - chunkRadius || chunk > _currentChunk + chunkRadius)) {
            DeactivateChunk(chunk);
        }
        for (int i = _currentChunk - chunkKeepRadius; i <= _currentChunk + chunkKeepRadius; i++) {
            ActivateChunk(i);
        }
    }

    private void ActivateChunk(int chunk) {
        if (_chunkMap.ContainsKey(chunk)) {
            return;
        }

        int randomValue = CantorPair(chunk, _seed);
        float xRaw = (randomValue % 211 - 211 / 2f) / (211 / 2f);
        float yRaw = (randomValue % 223 - 223 / 2f) / (223 / 2f);
        float scaleRaw = (randomValue % 227 - 227 / 2f) / (227 / 2f);
        Vector3 position = new Vector3(chunkRadius * (2 * chunk + xRaw / 2), 4f * yRaw, 0);
        var platform = Instantiate(platformPrefab, position, Quaternion.identity, platformParent.transform);
        var platformScale = platform.transform.localScale;
        platform.transform.localScale = new Vector3(
            platformScale.x + 10f - 20f * scaleRaw,
            platformScale.y,
            platformScale.z);
        _chunkMap.Add(chunk, new[] {platform});
    }

    private void DeactivateChunk(int chunk) {
        if (!_chunkMap.ContainsKey(chunk)) {
            return;
        }

        foreach (var platform in _chunkMap[chunk]) {
            Destroy(platform);
        }
        _chunkMap.Remove(chunk);
    }

    /**
     * Pair two integers to one
     */
    private int CantorPair(int x, int y) {
        x = Math.Abs(x + 1009);
        return (x + y) * (x + y + 1) / 2 + y;
    }

}
