## Regression
#### Your successses are reversible. Your failures are not.
---
##### My submission for the One Trick Pony Jam: https://itch.io/jam/one-trick-pony-jam
---
* Game jam theme: reversibility
* Game jam challenge: only use one skill
* Skill: programming

---
itch.io page: https://jfletcher94.itch.io/regression
